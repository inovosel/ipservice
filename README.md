# ip-service

Service for getting country name from the provided ip.

This project is created in **ECLIPSE**.
This is a spring boot project.

**Java version 1.8**

## Install instructions

To install this project pull it from repository using Clone. 
Then import it as exsisting maven project in your eclipse,
Or position yourself in cmd in directory where project is pulled **/pat/to/project/ip-service** and then run **mvn clean install** (not that maven needs to be instaled on your computer
if maen is not installed folow instructions here [](https://docs.wso2.com/display/IS323/Installing+Apache+Maven+on+Windows)),
after that postion yourself in **/path/to/project/ip-service/target** and run.

```cmd
java -jar ip-service.jar
```

Database used in this project is H2 in memory database.
To acsess this database console start project and go to http://localhost:8080/ip-service/h2-console.
And enter this information to connect to database.

![](./img/H2dbLogin.png)

<table>
    <thead>
        <tr>
            <th>Property Name</th>
            <th>Value</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td><b>Driver Class</b></td>
            <td>org.h2.Driver</td>
        </tr>
        <tr>
            <td><b>JDBC URL</b></td>
            <td>jdbc:h2:mem:IpCountryDB</td>
        </tr>
        <tr>
            <td><b>User Name</b></td>
            <td>sa</td>
        </tr>
        <tr>
            <td><b>Password</b></td>
            <td></td>
        </tr>
    </tbody>
</table>

Here you can run queries to inspect items in database.
To see items in table Countres just run:

```sql
SELECT * FROM COUNTRY 
```

To access available endpoints after starting project go to http://localhost:8080/ip-service/swagger-ui.html
Here you can see all available endpoints.

IpController is used to retrevie information about country for given ip.
Additionaly you can use this endpoint http://localhost:8080/ip-service/countries?ip=ENTER-IP-HERE to retrive same information.
Note that this is **GET** method.

operations-handler is used to retrive information about http traces to **/countries endpoint. This might not work since swagger wants to send body and 
endpoint does not accept empty body so this url mgit be better used to acsess this endpoint. 
http://localhost:8080/ip-service/actuator/httptrace.


# Changing settings

```yml
com:
  task:
    ipservice:
      ip-info-url: https://ipvigilante.com/
      trace-ignored-keywords:
      - actuator
      - v2
```

Properties above are located in application.yml and can be changed:

<table>
    <thead>
        <tr>
            <th>Property Name</th>
            <th>Action</th>
            <th>Note</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td><b>ip-info-url</b></td>
            <td>This is url from which information about country will be retrieved for given ip</td>
            <td>For the default endpoint /IP will be added in implementation.</td>
        </tr>
        <tr>
            <td><b>trace-ignored-keywords</b></td>
            <td>List of words that if are present in url will <b>not</b> be logged in http trace. </td>
            <td>If full url contains any of these words that url won't be logged in /httptrace.</td>
        </tr>
    </tbody>
</table>

    
# Changing default implementation
Default implementations (Currently IPVigilante implemntation is in class **com.task.ipservice.provider.IpVigilanteCountryProvider**)

To change this imeplemntation just change <b>ip-info-url</b> property if needed. 
Then create your own class that will implement **com.task.ipservice.provider.CountryProvider**

```java
public interface CountryProvider {
	public Country getCountryFromIp(String ip);
}
```

```java
public class IpVigilanteCountryProvider implements CountryProvider {
    public Country getCountryFromIp(String ip) {
        //Custom implementation....
    }
}
```
Then just change **com.task.ipservice.config.BeanConfig** to create your implementation bean.

```java
@Bean
public CountryProvider countryProvider() {
	return new CustomCountryProvider();
}
```