package com.task;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;

import com.task.ipservice.properties.GlobalProperties;

@SpringBootApplication
@EnableAutoConfiguration
@EnableConfigurationProperties(GlobalProperties.class)
public class IpServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(IpServiceApplication.class, args);
	}

}
