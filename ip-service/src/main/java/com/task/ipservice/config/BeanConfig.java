package com.task.ipservice.config;

import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;

import com.task.ipservice.exception.RestTempalteErrorHandler;
import com.task.ipservice.provider.CountryProvider;
import com.task.ipservice.provider.IpVigilanteCountryProvider;

@Configuration
public class BeanConfig {

	@Bean
	public RestTemplate restTempalte(RestTemplateBuilder restTemplateBuilder) {
		return restTemplateBuilder.errorHandler(new RestTempalteErrorHandler()).build();
	}

	@Bean
	public CountryProvider countryProvider() {
		return new IpVigilanteCountryProvider();
	}
}
