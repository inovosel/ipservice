package com.task.ipservice.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.task.ipservice.model.Country;
import com.task.ipservice.service.IpService;
import com.task.ipservice.validator.IpConstraint;

import io.swagger.annotations.Api;

@RestController
@Validated
@Api(value = "Controller for exposing /countries endpoint", tags = { "Ip Controller" })
public class IpController {

	@Autowired
	private IpService ipService;
	
	@GetMapping("/countries")
	public Country getCountryByIp(
			@RequestParam(name = "ip", required = true) @IpConstraint String ip) {
		return ipService.getCountryByIp(ip);
	}
}
