package com.task.ipservice.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.task.ipservice.model.Country;

@Repository
public class CountryRepository {
	@Autowired
	private JdbcTemplate jdbcTemplate;

	public int isertIntoCountry(Country entity) {
		if(entity != null) {
		   return jdbcTemplate.update("insert into country (ip, name) " + "values(?, ?)",
		        new Object[] {entity.getIp(), entity.getName()});
		} else {
			return 0;
		}
	}
}
