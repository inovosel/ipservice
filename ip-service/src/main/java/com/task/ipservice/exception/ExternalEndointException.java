package com.task.ipservice.exception;

import java.io.IOException;

public class ExternalEndointException extends IOException {

	private static final long serialVersionUID = -3851485889377004086L;
	
	public ExternalEndointException(String message) {
		super(message);
	}

}
