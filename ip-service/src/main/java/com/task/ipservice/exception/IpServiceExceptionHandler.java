package com.task.ipservice.exception;

import javax.validation.ConstraintViolationException;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import com.task.ipservice.model.ApiError;

@ControllerAdvice
public class IpServiceExceptionHandler {
	@ExceptionHandler
	public ResponseEntity<ApiError> handle(ConstraintViolationException exception) {
		ApiError error = new ApiError(HttpStatus.I_AM_A_TEAPOT.value(), exception.getMessage());
		return new ResponseEntity<ApiError>(error, null, HttpStatus.I_AM_A_TEAPOT);
	}
	
	@ExceptionHandler
	public ResponseEntity<ApiError> handle(ExternalEndointException exception) {
		ApiError error = new ApiError(HttpStatus.BAD_REQUEST.value(), exception.getMessage());
		return new ResponseEntity<ApiError>(error, null, HttpStatus.BAD_REQUEST);
	}

}
