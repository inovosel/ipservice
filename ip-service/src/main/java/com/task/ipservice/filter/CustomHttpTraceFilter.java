package com.task.ipservice.filter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.actuate.trace.http.HttpExchangeTracer;
import org.springframework.boot.actuate.trace.http.HttpTraceRepository;
import org.springframework.boot.actuate.web.trace.servlet.HttpTraceFilter;
import org.springframework.stereotype.Component;

import com.task.ipservice.properties.GlobalProperties;

@Component
public class CustomHttpTraceFilter extends HttpTraceFilter {
	
	@Autowired
	private GlobalProperties properties;
	
	public CustomHttpTraceFilter(HttpTraceRepository repository, HttpExchangeTracer tracer) {
		super(repository, tracer);
	}

	@Override
	protected boolean shouldNotFilter(HttpServletRequest request) throws ServletException {
		boolean shoudNotFilter = false;
		for(String filter : properties.getTraceIgnoredKeywords()) {
			if(request.getServletPath().contains(filter)) {
				shoudNotFilter = true;
				break;
			}
		}
		return shoudNotFilter;
	}
}
