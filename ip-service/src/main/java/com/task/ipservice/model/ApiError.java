package com.task.ipservice.model;

/**
 * class that will be returned if error in service occurs.
 * 
 * @author Ivan Novosel
 *
 */
public class ApiError {
	private int status;
	private String message;
	
	public ApiError(int status, String message) {
		super();
		this.setStatus(status);
		this.setMessage(message);
	}
	
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
}
