package com.task.ipservice.properties;

import java.util.List;

import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * This class contains configuration variables.
 * 
 * @author Ivan Novosel
 *
 */
@ConfigurationProperties(prefix = "com.task.ipservice")
public class GlobalProperties {
	
	/**
	 * External url endpoint that will be used to information about ip that is sent to /countries Endpoint.
	 */
	private String ipInfoUrl;
	/**
	 * If URL contains any of keywords defined here it will not be Traced by http tracing actuator.
	 */
	private List<String> traceIgnoredKeywords;

	public String getIpInfoUrl() {
		return ipInfoUrl;
	}
	public void setIpInfoUrl(String ipInfoUrl) {
		this.ipInfoUrl = ipInfoUrl;
	}
	public List<String> getTraceIgnoredKeywords() {
		return traceIgnoredKeywords;
	}
	public void setTraceIgnoredKeywords(List<String> traceIgnoredKeywords) {
		this.traceIgnoredKeywords = traceIgnoredKeywords;
	}

}
