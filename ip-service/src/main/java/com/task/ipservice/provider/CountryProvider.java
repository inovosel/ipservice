package com.task.ipservice.provider;

import com.task.ipservice.model.Country;

/**
 * This interface will be used to create {@link Country} object.
 * <p>
 * To Create new provider just inherit this interface and create bean for your implementation.
 * 
 * @author Ivan Novosel
 *
 */
public interface CountryProvider {
	public Country getCountryFromIp(String ip);
}
