package com.task.ipservice.provider;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.web.client.RestTemplate;

import com.task.ipservice.model.Country;
import com.task.ipservice.properties.GlobalProperties;

public class IpVigilanteCountryProvider implements CountryProvider {

	@Autowired
	private GlobalProperties properties;
	
	@Autowired
	private RestTemplate restTemplate;
	
	@SuppressWarnings("unchecked")
	@Override
	public Country getCountryFromIp(String ip) {
		Map<String, Object> countryInfo = getCountryInfoFromEndpoint(ip);
		if(countryInfo != null && countryInfo.get("status").equals("success")) {
			Map<String, Object> data = (Map<String, Object>) countryInfo.get("data");
			return new Country(ip, data.get("country_name").toString());
		} 
		return null;
	}
	
	public Map<String, Object> getCountryInfoFromEndpoint(String ip){
		HttpHeaders headers = new HttpHeaders();
		return restTemplate.exchange(properties.getIpInfoUrl() + ip, HttpMethod.GET, new HttpEntity<String>(headers), 
				new ParameterizedTypeReference<Map<String, Object>>() {}).getBody();
	}

}
