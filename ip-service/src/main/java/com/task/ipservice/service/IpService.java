package com.task.ipservice.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.task.ipservice.dao.CountryRepository;
import com.task.ipservice.model.Country;
import com.task.ipservice.provider.CountryProvider;

/**
 * 
 * This service will provide {@link Country} object for given ip.
 * <p>
 * To get {@link Country} object bean of type {@link CountryProvider} will be used.
 * 
 * @author Ivan Novosel
 *
 */
@Service
public class IpService {
	
	@Autowired
	private CountryProvider provider;
	
	@Autowired
	private CountryRepository countryRepository; 
	
	public Country getCountryByIp(String ip) {
		Country value;
		value = provider.getCountryFromIp(ip);
		countryRepository.isertIntoCountry(value);
		return value;
	}
}
