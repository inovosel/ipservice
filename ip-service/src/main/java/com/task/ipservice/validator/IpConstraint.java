package com.task.ipservice.validator;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.Payload;

@Documented
@Constraint(validatedBy = IpValidator.class)
@Target({ ElementType.METHOD, ElementType.FIELD, ElementType.PARAMETER })
@Retention(RetentionPolicy.RUNTIME)
public @interface IpConstraint {
	String message() default "Ip adress you entered is not in valid format valid format is {0-255}.{0-255}.{0-255}.{0-255}";
	Class<?>[] groups() default {};
	Class<? extends Payload>[] payload() default {};
}
